﻿// ===================================================
// Code Example A / CodeExampleA.Web / IUserService.cs
// Created on 06/03/2014 @ 10:44 PM
// ===================================================

namespace CodeExampleA.Web.Code.Services
{
    using System.Collections.Generic;
    using CodeExampleA.Web.Models.User;

    public interface IUserService
    {
        #region Public Methods and Operators

        List<UserInfo> GetAllUsers();
        UserInfo SaveUser(string firstName, string lastName, int age);

        #endregion
    }
}
