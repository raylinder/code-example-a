﻿// ===================================================
// Code Example A / CodeExampleA.Web / UserService.cs
// Created on 06/03/2014 @ 10:26 PM
// ===================================================

namespace CodeExampleA.Web.Code.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using CodeExampleA.Domains;
    using CodeExampleA.Web.Models.User;

    public class UserService : IUserService
    {
        #region Constructors and Destructors

        public UserService(IUserInfoDomain userInfoDomain)
        {
            UserInfoDomain = userInfoDomain;
        }

        public UserService()
        {
        }

        #endregion

        #region Properties

        protected IUserInfoDomain UserInfoDomain { get; private set; }

        #endregion

        #region Public Methods and Operators

        public List<UserInfo> GetAllUsers()
        {
            //var userDomain = new UserInfoDomain();
            //var userResult = userDomain.GetAllUserInfo();

            return new UserInfoDomain().GetAllUserInfo().Select(userInfo => new UserInfo { Id = userInfo.id, Age = userInfo.age, FirstName = userInfo.firstName, LastName = userInfo.lastName }).ToList();
        }

        public UserInfo SaveUser(string firstName, string lastName, int age)
        {
            //var userDomain = new UserInfoDomain();
            //var userResult = userDomain.AddUserInfo(firstName, lastName, age);
            //var result = new UserInfo { Id = userResult.id };

            var userResult = new UserInfoDomain().AddUserInfo(firstName, lastName, age);
            return new UserInfo { Id = userResult.id };
        }

        #endregion
    }
}
