﻿// ===================================================
// Code Example A / CodeExampleA.Web / UserInfo.cs
// Created on 06/03/2014 @ 10:21 PM
// ===================================================

namespace CodeExampleA.Web.Models.User
{
    using System;

    public class UserInfo
    {
        #region Public Properties

        public int Age { get; set; }
        public string FirstName { get; set; }
        public Guid Id { get; set; }
        public string LastName { get; set; }

        #endregion
    }
}
