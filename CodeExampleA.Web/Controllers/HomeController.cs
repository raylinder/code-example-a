﻿// ===================================================
// Code Example A / CodeExampleA.Web / HomeController.cs
// Created on 06/02/2014 @ 9:55 PM
// ===================================================

namespace CodeExampleA.Web.Controllers
{
    using System.Web.Mvc;

    public class HomeController : Controller
    {
        #region Public Methods and Operators

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        #endregion
    }
}
