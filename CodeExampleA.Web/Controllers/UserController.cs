﻿// ===================================================
// Code Example A / CodeExampleA.Web / UserController.cs
// Created on 06/03/2014 @ 10:24 PM
// ===================================================

namespace CodeExampleA.Web.Controllers
{
    using System.Web.Http;
    using CodeExampleA.Web.Code.Services;
    using CodeExampleA.Web.Models.User;

    public class UserController : ApiController
    {
        #region Public Methods and Operators

        public IHttpActionResult GetAllUsers()
        {
            //var userService = new UserService();
            //var returnData = userService.GetAllUsers();
            return Ok(new UserService().GetAllUsers());
        }

        public IHttpActionResult SaveUser(UserInfo userInfo)
        {
            if (userInfo == null)
            {
                return NotFound();
                //throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound)); // for WEB API 2, it's just - NotFound();
            }

            //var userService = new UserService();
            //var returnData = userService.SaveUser(userInfo.FirstName, userInfo.LastName, userInfo.Age);
            return Ok(new UserService().SaveUser(userInfo.FirstName, userInfo.LastName, userInfo.Age));
        }

        #endregion
    }
}
