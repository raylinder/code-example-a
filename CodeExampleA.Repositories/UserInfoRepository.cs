﻿// ===================================================
// Code Example A / CodeExampleA.Repositories / UserInfoRepository.cs
// Created on 06/03/2014 @ 1:15 AM
// ===================================================

namespace CodeExampleA.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using CodeExampleA.Entities;
    using CodeExampleA.Extensions.Data;

    public class UserInfoRepository : IUserInfoRepository
    {
        #region Constants

        private const string SpGetAllUserInfo = "dbo.GetAllUserInfo";
        private const string SpAddUserInfo = "dbo.AddUserInfo";
        private const string ConnectionStringName = "CodeExampleA";

        #endregion

        #region Fields

        private readonly string connString = ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString;

        #endregion

        #region Public Methods and Operators

        public IEnumerable<UserInfo> GetAllUserInfo()
        {
            var retVal = new List<UserInfo>();
            using (var conn = new SqlConnection(connString))
            {
                using (var cmd = new SqlCommand(SpGetAllUserInfo, conn))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure; conn.Open();
                        using (var rdr = cmd.ExecuteReader())
                        {
                            while (rdr.Read())
                                retVal.Add(UserInfo.FromReader(rdr));

                            rdr.Close();
                        }
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }

            return retVal;
        }

        public Guid? AddUserInfo(string firstName, string lastName, int age, out Guid? id)
        {
            Guid retVal;

            using (var conn = new SqlConnection(connString))
            {
                using (var cmd = new SqlCommand(SpAddUserInfo, conn))
                {
                    try
                    {
                        cmd.Parameters.AddWithValue_Nullable("@firstName", firstName);
                        cmd.Parameters.AddWithValue_Nullable("@lastName", lastName);
                        cmd.Parameters.AddWithValue_Nullable("@age", age);
                        cmd.Parameters["@id"].Direction = ParameterDirection.Output;
                        cmd.CommandType = CommandType.StoredProcedure;
                        conn.Open();

                        retVal = (Guid)cmd.ExecuteScalar();

                        id = (Guid?)cmd.Parameters["@id"].Value.DbNullable();
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }

            return retVal;
        }

        #endregion
    }
}
