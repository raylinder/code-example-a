﻿// ===================================================
// Code Example A / CodeExampleA / IUserInfoDomain.cs
// Created on 06/03/2014 @ 9:21 PM
// ===================================================

namespace CodeExampleA.Domains
{
    using CodeExampleA.Entities;

    public interface IUserInfoDomain
    {
        UserInfo AddUserInfo(string firstName, string lastName, int age);
    }
}
