﻿// ===================================================
// Code Example A / CodeExampleA / IUserInfoRepository.cs
// Created on 06/03/2014 @ 9:19 PM
// ===================================================

namespace CodeExampleA.Repositories
{
    using System;
    using System.Collections.Generic;
    using CodeExampleA.Entities;

    public interface IUserInfoRepository
    {
        #region Public Methods and Operators

        Guid? AddUserInfo(string firstName, string lastName, int age, out Guid? id);
        IEnumerable<UserInfo> GetAllUserInfo();

        #endregion
    }
}
