﻿// ===================================================
// Code Example A / CodeExampleA.Extensions.Core / ExceptionDetail.cs
// Created on 06/02/2014 @ 11:48 PM
// ===================================================

namespace CodeExampleA.Extensions
{
    using System;

    /// <summary>
    ///     Stores details about an exception.
    /// </summary>
    public class ExceptionDetail<T>
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Constructor.
        /// </summary>
        /// <param name="source">The object which caused the exception.</param>
        /// <param name="exception">The exception which was thrown.</param>
        public ExceptionDetail(T source, Exception exception)
        {
            Source = source;
            Exception = exception;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the exception which was thrown.
        /// </summary>
        public Exception Exception { get; private set; }

        /// <summary>
        ///     Gets the object which caused the exception.
        /// </summary>
        public T Source { get; private set; }

        #endregion
    }
}
