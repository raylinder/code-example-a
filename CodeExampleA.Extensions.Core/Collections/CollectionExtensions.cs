﻿// ===================================================
// Code Example A / CodeExampleA.Extensions.Core / CollectionExtensions.cs
// Created on 06/02/2014 @ 11:57 PM
// ===================================================

namespace CodeExampleA.Extensions
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Text.RegularExpressions;

    /// <summary>
    ///     Provided extended methods for collections.
    /// </summary>
    /// <summary>
    ///     Provided extended methods for collections.
    /// </summary>
    public static class CollectionExtensions
    {
        #region Static Fields

        private static readonly Regex nonAlphaNum = new Regex("[^A-Z0-9 ]", RegexOptions.Compiled);
        private static readonly char[] splitChars = { ' ' };

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Adds a Key Value Pair to a Name Value Collection
        /// </summary>
        /// <param name="target">The Name Value Collection to which the key/value pair will be added.</param>
        /// <param name="item">The key/value pair to be added to the collection</param>
        public static void Add(this NameValueCollection target, KeyValuePair<string, string> item)
        {
            if (target == null)
            {
                throw new ArgumentNullException("target");
            }

            target.Add(item.Key, item.Value);
        }

        /// <summary>
        ///     Adds a Key Value Pair to a Dictionary
        /// </summary>
        /// <param name="dictionary">The Name Value Collection to which the key/value pair will be added.</param>
        /// <param name="item">The key/value pair to be added to the collection</param>
        public static void Add<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, KeyValuePair<TKey, TValue> item)
        {
            if (dictionary == null)
            {
                throw new ArgumentNullException("dictionary");
            }

            dictionary.Add(item.Key, item.Value);
        }

        /// <summary>
        ///     Adds an item to the collection if it matches the specified condition.
        /// </summary>
        /// <typeparam name="T">The type of the elements stored in the collection.</typeparam>
        /// <param name="list">The collection.</param>
        /// <param name="item">The item to be added.</param>
        /// <param name="match">The condition to be used to evaluate the item.</param>
        [DebuggerNonUserCode]
        public static void AddIf<T>(this IList<T> list, T item, Predicate<T> match)
        {
            if (list == null)
            {
                throw new ArgumentNullException("list");
            }
            if (match == null)
            {
                throw new ArgumentNullException("match");
            }

            if (match(item))
            {
                list.Add(item);
            }
        }

        /// <summary>
        ///     Adds an enumeration of key/value pairs to a Name Value Collection.
        /// </summary>
        /// <param name="target">The Name Value Collection to which the items in the collection will be added.</param>
        /// <param name="collection">The collection of key value pairs to be added to the Name Value Collection</param>
        public static void AddRange(this NameValueCollection target, IEnumerable<KeyValuePair<string, string>> collection)
        {
            if (target == null)
            {
                throw new ArgumentNullException("target");
            }
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }

            collection.ForEach(target.Add);
        }

        /// <summary>
        ///     Adds a collection of keys and a collection of values to a Dictionary.
        /// </summary>
        /// <typeparam name="TKey">The type of the Dictionary Key</typeparam>
        /// <typeparam name="TValue">The type of the Dictionary Value</typeparam>
        /// <param name="dictionary">The Dictionary.</param>
        /// <param name="keys">The collection of keys</param>
        /// <param name="values">The collection of values</param>
        [DebuggerNonUserCode]
        public static void AddRange<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, IEnumerable<TKey> keys, IEnumerable<TValue> values)
        {
            if (dictionary == null)
            {
                throw new ArgumentNullException("dictionary");
            }
            if (keys == null)
            {
                throw new ArgumentNullException("keys");
            }
            if (values == null)
            {
                throw new ArgumentNullException("values");
            }
            var enumerableValues = values as IList<TValue> ?? values.ToList();
            var enumerableKeys = keys as IList<TKey> ?? keys.ToList();
            if (enumerableValues.Count() != enumerableKeys.Count())
            {
                throw new ArgumentException("The number of keys does not match the number of values.");
            }

            var keysEnum = enumerableKeys.GetEnumerator();
            var valuesEnum = enumerableValues.GetEnumerator();

            while (keysEnum.MoveNext() && valuesEnum.MoveNext())
            {
                dictionary.Add(keysEnum.Current, valuesEnum.Current);
            }
        }

        /// <summary>
        ///     Adds a collection of key/value pairs to a Dictionary.
        /// </summary>
        /// <typeparam name="TKey">The type of the Dictionary Key</typeparam>
        /// <typeparam name="TValue">The type of the Dictionary Value</typeparam>
        /// <param name="dictionary">The Dictionary.</param>
        /// <param name="values">The collection of key value pairs</param>
        [DebuggerNonUserCode]
        public static void AddRange<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, IEnumerable<KeyValuePair<TKey, TValue>> values)
        {
            if (dictionary == null)
            {
                throw new ArgumentNullException("dictionary");
            }
            if (values == null)
            {
                throw new ArgumentNullException("values");
            }

            var valuesEnum = values.GetEnumerator();

            while (valuesEnum.MoveNext())
            {
                dictionary.Add(valuesEnum.Current.Key, valuesEnum.Current.Value);
            }
        }

        /// <summary>
        ///     Adds a collection of items matching the specified condition to a collection.
        /// </summary>
        /// <typeparam name="T">The type of the elements stored in the collection.</typeparam>
        /// <param name="list">The collection.</param>
        /// <param name="collection">The collection of items to be inspected and added to the collection.</param>
        /// <param name="match">The condition to be used to evaluate the item.</param>
        [DebuggerNonUserCode]
        public static void AddRangeIf<T>(this IList<T> list, IEnumerable<T> collection, Predicate<T> match)
        {
            if (list == null)
            {
                throw new ArgumentNullException("list");
            }
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }

            collection.ForEach(item => list.AddIf(item, match));
        }

        /// <summary>
        ///     Calculates the edit distance between two Enumerable objects.
        /// </summary>
        /// <typeparam name="T">The type of Enumerable object.</typeparam>
        /// <param name="source">The source Enumerable object.</param>
        /// <param name="target">The target Enumerable object.</param>
        /// <returns>A count of how many adds/deletes/changes are required to modify the Source to match the Target.</returns>
        [DebuggerNonUserCode]
        public static int CalculateEditDistance<T>(this IEnumerable<T> source, IEnumerable<T> target) where T : IEquatable<T>
        {
            // Validate parameters
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            if (target == null)
            {
                throw new ArgumentNullException("target");
            }

            // Convert the parameters into IList instances
            // in order to obtain indexing capabilities
            var first = source as IList<T> ?? new List<T>(source);
            var second = target as IList<T> ?? new List<T>(target);

            // Get the length of both.  If either is 0, return
            // the length of the other, since that number of insertions
            // would be required.
            int n = first.Count, m = second.Count;
            if (n == 0)
            {
                return m;
            }
            if (m == 0)
            {
                return n;
            }

            // Rather than maintain an entire matrix (which would require O(n*m) space),
            // just store the current row and the next row, each of which has a length m+1,
            // so just O(m) space. Initialize the current row.
            int curRow = 0, nextRow = 1;
            var rows = new[] { new int[m + 1], new int[m + 1] };
            for (var j = 0; j <= m; ++j)
            {
                rows[curRow][j] = j;
            }

            // For each virtual row (since we only have physical storage for two)
            for (var i = 1; i <= n; ++i)
            {
                // Fill in the values in the row
                rows[nextRow][0] = i;
                for (var j = 1; j <= m; ++j)
                {
                    var dist1 = rows[curRow][j] + 1;
                    var dist2 = rows[nextRow][j - 1] + 1;
                    var dist3 = rows[curRow][j - 1] + (first[i - 1].Equals(second[j - 1]) ? 0 : 1);

                    rows[nextRow][j] = Math.Min(dist1, Math.Min(dist2, dist3));
                }

                // Swap the current and next rows
                if (curRow == 0)
                {
                    curRow = 1;
                    nextRow = 0;
                }
                else
                {
                    curRow = 0;
                    nextRow = 1;
                }
            }

            // Return the computed edit distance
            return rows[curRow][m];
        }

        /// <summary>
        ///     Dequeues a number of elements from a queue.
        /// </summary>
        /// <typeparam name="T">The type of elements in the queue and collection.</typeparam>
        /// <param name="queue">The queue to which the elements will be enqueued.</param>
        /// <param name="count">The number of elements to be dequeued.</param>
        /// <returns>Returns an array containing the elements which have been dequeued.</returns>
        [DebuggerNonUserCode]
        public static T[] Dequeue<T>(this Queue<T> queue, int count)
        {
            if (queue == null)
            {
                throw new ArgumentNullException("queue");
            }
            if (count <= 0)
            {
                throw new ArgumentOutOfRangeException("count");
            }

            if (count > queue.Count)
            {
                count = queue.Count;
            }

            var retVal = new T[count];

            for (var idx = 0; idx < retVal.Length; idx++)
            {
                retVal[idx] = queue.Dequeue();
            }

            return retVal;
        }

        /// <summary>
        ///     Enqueues an enumerable collection of elements into the queue.
        /// </summary>
        /// <typeparam name="T">The type of elements in the queue and collection.</typeparam>
        /// <param name="queue">The queue to which the elements will be enqueued.</param>
        /// <param name="collection">The collection containing the elements to be enqueued.</param>
        [DebuggerNonUserCode]
        public static void Enqueue<T>(this Queue<T> queue, IEnumerable<T> collection)
        {
            collection.ForEach(queue.Enqueue);
        }

        /// <summary>
        ///     Determines whether the collection contains elements that match the conditions defined by the specified predicate.
        /// </summary>
        /// <param name="collection">The collection of elements to be searched.</param>
        /// <param name="match">The System.Predicate&lt;T&gt; delegate that defines the conditions of the elements to search for.</param>
        /// <returns>Returns true of any elements match the conditions defined by the specified predicate.</returns>
        /// <exception cref="System.ArgumentNullException">System.ArgumentNullException</exception>
        [DebuggerNonUserCode]
        public static bool Exists<T>(this IEnumerable<T> collection, Predicate<T> match) where T : IEquatable<T>
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }
            if (match == null)
            {
                throw new ArgumentNullException("match");
            }

            return !Equals(Find(collection, match), default(T));
        }

        /// <summary>
        ///     Searches for an element that matches the conditions defined by the specified predicate and returns the first
        ///     occurence within the entire collection.
        /// </summary>
        /// <param name="collection">The collection of elements to be searched.</param>
        /// <param name="match">The System.Predicate&lt;T&gt; delegate that defines the conditions of the elements to search for.</param>
        /// <returns>Returns the first occurence within the entire collection.</returns>
        /// <exception cref="System.ArgumentNullException">System.ArgumentNullException</exception>
        [DebuggerNonUserCode]
        public static T Find<T>(this IEnumerable<T> collection, Predicate<T> match)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }
            if (match == null)
            {
                throw new ArgumentNullException("match");
            }

            var retVal = default(T);
            foreach (var item in collection)
            {
                if (match(item))
                {
                    retVal = item;
                    break;
                }
            }
            return retVal;
        }

        /// <summary>
        ///     Returns all of the elements that match the conditions defined by the specified predicate.
        /// </summary>
        /// <param name="collection">The collection of elements to be searched.</param>
        /// <param name="match">The System.Predicate&lt;T&gt; delegate that defines the conditions of the elements to search for.</param>
        /// <returns>Returns all of the elements that match the conditions defined by the specified predicate.</returns>
        /// <exception cref="System.ArgumentNullException">System.ArgumentNullException</exception>
        [DebuggerNonUserCode]
        public static List<T> FindAll<T>(this IEnumerable<T> collection, Predicate<T> match)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }
            if (match == null)
            {
                throw new ArgumentNullException("match");
            }

            var retVal = new List<T>();
            retVal.AddRangeIf(collection, match);
            return retVal;
        }

        /// <summary>
        ///     Performs the specified action on each element of the collection.
        /// </summary>
        /// <typeparam name="T">The type of elements in the collection.</typeparam>
        /// <param name="collection">The collection of elements upon which the action will be performed.</param>
        /// <param name="action">The delegate to perform on each element of the collection.</param>
        /// <exception cref="System.ArgumentNullException">System.ArgumentNullException</exception>
        [DebuggerNonUserCode]
        public static void ForEach<T>(this IEnumerable<T> collection, Action<T> action)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }
            foreach (var item in collection)
            {
                action(item);
            }
        }

        /// <summary>
        ///     Performs the specified action on each element of the collection.
        /// </summary>
        /// <typeparam name="T">The type of elements in the collection.</typeparam>
        /// <param name="collection">The collection of elements upon which the action will be performed.</param>
        /// <param name="action">The delegate to perform on each element of the collection.</param>
        /// <exception cref="System.ArgumentNullException">System.ArgumentNullException</exception>
        [DebuggerNonUserCode]
        public static void ForEach<T>(this IEnumerable collection, Action<T> action)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }
            foreach (T item in collection)
            {
                action(item);
            }
        }

        /// <summary>
        ///     Performs the specified action on each element of the collection even if an exception is thrown for one of the
        ///     elements.
        /// </summary>
        /// <typeparam name="T">The type of elements in the collection.</typeparam>
        /// <param name="collection">The collection of elements upon which the action will be performed.</param>
        /// <param name="action">The delegate to perform on each element of the collection.</param>
        /// <exception cref="System.ArgumentNullException">System.ArgumentNullException</exception>
        /// <returns>Returns an array of exceptions caught while performing the action on each element of the collection.</returns>
        [DebuggerNonUserCode]
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes",
            Justification = "It doesn't matter what exceptions might be thrown here.  Any exception thrown needs to be handled the same way: add to exceptions detail.")]
        public static ExceptionDetail<T>[] ForEachGuaranteed<T>(this IEnumerable<T> collection, Action<T> action)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            var exceptions = new List<ExceptionDetail<T>>();

            foreach (var item in collection)
            {
                try
                {
                    action(item);
                }
                catch (Exception ex)
                {
                    exceptions.Add(new ExceptionDetail<T>(item, ex));
                }
            }

            return exceptions.ToArray();
        }

        /// <summary>
        ///     Performs the specified action on each element of the collection even if an exception is thrown for one of the
        ///     elements.
        /// </summary>
        /// <typeparam name="T">The type of elements in the collection.</typeparam>
        /// <param name="collection">The collection of elements upon which the action will be performed.</param>
        /// <param name="action">The delegate to perform on each element of the collection.</param>
        /// <exception cref="System.ArgumentNullException">System.ArgumentNullException</exception>
        /// <returns>Returns an array of exceptions caught while performing the action on each element of the collection.</returns>
        [DebuggerNonUserCode]
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes",
            Justification = "It doesn't matter what exceptions might be thrown here.  Any exception thrown needs to be handled the same way: add to exceptions detail.")]
        public static ExceptionDetail<T>[] ForEachGuaranteed<T>(this IEnumerable collection, Action<T> action)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            var exceptions = new List<ExceptionDetail<T>>();

            foreach (T item in collection)
            {
                try
                {
                    action(item);
                }
                catch (Exception ex)
                {
                    exceptions.Add(new ExceptionDetail<T>(item, ex));
                }
            }

            return exceptions.ToArray();
        }

        /// <summary>
        ///     Performs the specified action on each element of the collection even if an exception is thrown for one of the
        ///     elements.
        /// </summary>
        /// <typeparam name="T">The type of elements in the collection.</typeparam>
        /// <param name="collection">The collection of elements upon which the action will be performed.</param>
        /// <param name="validator">The validator to be used to test the item.</param>
        /// <param name="action">The delegate to perform on each element of the collection.</param>
        /// <exception cref="System.ArgumentNullException">System.ArgumentNullException</exception>
        /// <returns>Returns an array of exceptions caught while performing the action on each element of the collection.</returns>
        [DebuggerNonUserCode]
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes",
            Justification = "It doesn't matter what exceptions might be thrown here.  Any exception thrown needs to be handled the same way: add to exceptions detail.")]
        public static ExceptionDetail<T>[] ForEachGuaranteedIf<T>(this IEnumerable<T> collection, Predicate<T> validator, Action<T> action)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }
            if (validator == null)
            {
                throw new ArgumentNullException("validator");
            }

            var exceptions = new List<ExceptionDetail<T>>();

            foreach (var item in collection)
            {
                try
                {
                    if (validator(item))
                    {
                        action(item);
                    }
                }
                catch (Exception ex)
                {
                    exceptions.Add(new ExceptionDetail<T>(item, ex));
                }
            }

            return exceptions.ToArray();
        }

        /// <summary>
        ///     Performs the specified action on each element of the collection even if an exception is thrown for one of the
        ///     elements.
        /// </summary>
        /// <typeparam name="T">The type of elements in the collection.</typeparam>
        /// <param name="collection">The collection of elements upon which the action will be performed.</param>
        /// <param name="validator">The validator to be used to test the item.</param>
        /// <param name="action">The delegate to perform on each element of the collection.</param>
        /// <exception cref="System.ArgumentNullException">System.ArgumentNullException</exception>
        /// <returns>Returns an array of exceptions caught while performing the action on each element of the collection.</returns>
        [DebuggerNonUserCode]
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes",
            Justification = "It doesn't matter what exceptions might be thrown here.  Any exception thrown needs to be handled the same way: add to exceptions detail.")]
        public static ExceptionDetail<T>[] ForEachGuaranteedIf<T>(this IEnumerable collection, Predicate<T> validator, Action<T> action)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }
            if (validator == null)
            {
                throw new ArgumentNullException("validator");
            }

            var exceptions = new List<ExceptionDetail<T>>();

            foreach (T item in collection)
            {
                try
                {
                    if (validator(item))
                    {
                        action(item);
                    }
                }
                catch (Exception ex)
                {
                    exceptions.Add(new ExceptionDetail<T>(item, ex));
                }
            }

            return exceptions.ToArray();
        }

        /// <summary>
        ///     Performs the specified action on each element of the collection.
        /// </summary>
        /// <typeparam name="T">The type of elements in the collection.</typeparam>
        /// <param name="collection">The collection of elements upon which the action will be performed.</param>
        /// <param name="validator">The validator to be used to test the item.</param>
        /// <param name="action">The delegate to perform on each element of the collection.</param>
        /// <exception cref="System.ArgumentNullException">System.ArgumentNullException</exception>
        [DebuggerNonUserCode]
        public static void ForEachIf<T>(this IEnumerable<T> collection, Predicate<T> validator, Action<T> action)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }
            if (validator == null)
            {
                throw new ArgumentNullException("validator");
            }
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }
            foreach (var item in collection)
            {
                if (validator(item))
                {
                    action(item);
                }
            }
        }

        /// <summary>
        ///     Performs the specified action on each element of the collection.
        /// </summary>
        /// <typeparam name="T">The type of elements in the collection.</typeparam>
        /// <param name="collection">The collection of elements upon which the action will be performed.</param>
        /// <param name="validator">The validator to be used to test the item.</param>
        /// <param name="action">The delegate to perform on each element of the collection.</param>
        /// <exception cref="System.ArgumentNullException">System.ArgumentNullException</exception>
        [DebuggerNonUserCode]
        public static void ForEachIf<T>(this IEnumerable collection, Predicate<T> validator, Action<T> action)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }
            if (validator == null)
            {
                throw new ArgumentNullException("validator");
            }
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }
            foreach (T item in collection)
            {
                if (validator(item))
                {
                    action(item);
                }
            }
        }

        /// <summary>
        ///     Generates a score based on Edit Distance comparison.
        /// </summary>
        /// <param name="source">The source string to compare.</param>
        /// <param name="target">The target string to compare.</param>
        /// <returns>Returns a score of zero (0) to one hundred (100) based on Edit Distance comparison.</returns>
        /// <remarks>
        ///     A score of zero (0) indicates the two strings are not alike.
        ///     A score of one hundred (100) indicates the two strings contain identical words (regardless of order).
        ///     Missing elements are assessed a penalty of 5 points.
        ///     "John Michael" compared to "Michael John" = 100.
        ///     "John Alan Michael" compared to "John Michael" = 95.
        ///     "aaa bbb ccc" compared to "xxx yyy zzz" = 0.
        /// </remarks>
        [DebuggerNonUserCode]
        public static int GetEditDistanceScore(this string source, string target)
        {
            return GetEditDistanceScore(source, target, 5);
        }

        /// <summary>
        ///     Generates a score based on Edit Distance comparison.
        /// </summary>
        /// <param name="source">The source string to compare.</param>
        /// <param name="target">The target string to compare.</param>
        /// <param name="missingItemPenalty">The penalty to assess for missing elements.</param>
        /// <returns>Returns a score of zero (0) to one hundred (100) based on Edit Distance comparison.</returns>
        /// <remarks>
        ///     A score of zero (0) indicates the two strings are not alike.
        ///     A score of one hundred (100) indicates the two strings contain identical words (regardless of order).
        ///     Missing elements are assessed a penalty dependent upon the missingItemPenalty argument.
        ///     E.g. for a missingItemPenalty of 5:
        ///     "John Michael" compared to "Michael John" = 100.
        ///     "John Alan Michael" compared to "John Michael" = 95.
        ///     "aaa bbb ccc" compared to "xxx yyy zzz" = 0.
        /// </remarks>
        /// <exception cref="System.ArgumentNullException">Thrown when either the source or target is null.</exception>
        [DebuggerNonUserCode]
        public static int GetEditDistanceScore(this string source, string target, int missingItemPenalty)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            if (target == null)
            {
                throw new ArgumentNullException("target");
            }

            int retVal;

            if (source.Equals(target, StringComparison.OrdinalIgnoreCase))
            {
                retVal = 100;
            }
            else
            {
                source = nonAlphaNum.Replace(source.ToUpperInvariant(), " ");
                target = nonAlphaNum.Replace(target.ToUpperInvariant(), " ");

                var sourceTokens = source.Split(splitChars, StringSplitOptions.RemoveEmptyEntries);
                var targetTokens = target.Split(splitChars, StringSplitOptions.RemoveEmptyEntries);

                var numerator = 0;
                int denominator;
                var penalty = 0;

                if ((sourceTokens.Length == 1) || (targetTokens.Length == 1))
                {
                    source = source.Replace(" ", string.Empty);
                    target = target.Replace(" ", string.Empty);
                    numerator = GetScore(source, target);
                    denominator = 1;
                }
                else
                {
                    var scores = new int[sourceTokens.Length][];

                    for (var idxSource = 0; idxSource < sourceTokens.Length; idxSource++)
                    {
                        scores[idxSource] = new int[targetTokens.Length];
                        for (var idxTarget = 0; idxTarget < targetTokens.Length; idxTarget++)
                        {
                            scores[idxSource][idxTarget] = GetScore(sourceTokens[idxSource], targetTokens[idxTarget]);
                        }
                    }

                    var sourceIdx = -1;
                    var targetIdx = -1;
                    var bestScores = new int[Math.Max(sourceTokens.Length, targetTokens.Length)];

                    foreach (var score in sourceTokens.Select(t => GetBestScore(ref sourceIdx, ref targetIdx, scores, sourceTokens.Length, targetTokens.Length)).Where(score => score > 0))
                    {
                        bestScores[sourceIdx] = score;

                        for (var idxSource = 0; idxSource < sourceTokens.Length; idxSource++)
                        {
                            scores[idxSource][targetIdx] = 0;
                        }

                        for (var idxTarget = 0; idxTarget < targetTokens.Length; idxTarget++)
                        {
                            scores[sourceIdx][idxTarget] = 0;
                        }
                    }

                    numerator += bestScores.Sum();
                    denominator = Math.Min(sourceTokens.Length, targetTokens.Length);
                    penalty = (Math.Abs(sourceTokens.Length - targetTokens.Length) * missingItemPenalty);
                }

                retVal = (numerator / denominator) - penalty;
            }

            return (retVal < 0) ? 0 : retVal;
        }

        /// <summary>
        ///     Test to see if the specified array is null or contains no elements.
        /// </summary>
        /// <param name="array">The array to be tested.</param>
        /// <returns>Returns true if the array is null or contains no elements.</returns>
        [DebuggerNonUserCode]
        public static bool IsNullOrEmpty(this Array array)
        {
            return ((array == null) || (array.Length == 0));
        }

        /// <summary>
        ///     Sorts a collection of strings based on their Edit Distance Score to the specified source.
        /// </summary>
        /// <param name="collection">The collection of strings to be sorted.</param>
        /// <param name="source">The source to which each item of the collection will be compared using the Edit Distance Score.</param>
        /// <returns>Returns a List of string sorted by their score</returns>
        [DebuggerNonUserCode]
        public static List<string> SortByEditDistanceScore(this IEnumerable<string> collection, string source)
        {
            var scores = new List<EditDistanceScore>();
            collection.ForEach(item => scores.Add(new EditDistanceScore { Value = item, Score = source.GetEditDistanceScore(item) }));
            scores.Sort(EditDistanceScoreComparison);

            var retVal = new List<string>();
            scores.ForEach(item => retVal.Add(item.Value));
            return retVal;
        }

        /// <summary>
        ///     Determines whether every element in the List matches the conditions defined by the specified predicate.
        /// </summary>
        /// <param name="collection">The collection of elements to be tested.</param>
        /// <param name="match">The System.Predicate&lt;T&gt; delegate that defines the conditions to check against the elements.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">System.ArgumentNullException</exception>
        [DebuggerNonUserCode]
        public static bool TrueForAll<T>(this IEnumerable<T> collection, Predicate<T> match)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }
            if (match == null)
            {
                throw new ArgumentNullException("match");
            }

            return collection.All(item => match(item));
        }

        /// <summary>
        ///     Checks a string's length and truncates if the string is longer than the maximum value.
        /// </summary>
        /// <param name="input">The string whose length is to be validated.</param>
        /// <param name="maxLength">The maximum length of the return value.</param>
        /// <returns>Returns the input string up to the maximum length specified.</returns>
        /// <remarks>
        ///     If the maximum length specified is zero or less than zero, then the input value will be returned without being
        ///     truncated.
        /// </remarks>
        [DebuggerNonUserCode]
        public static string Truncate(this string input, int maxLength)
        {
            if ((maxLength <= 0) || string.IsNullOrEmpty(input) || (input.Length <= maxLength))
            {
                return input;
            }
            else
            {
                return input.Substring(0, maxLength);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Comparison method used for sorting Edit Distance Scores.
        /// </summary>
        /// <param name="source">The source for comparison.</param>
        /// <param name="target">The target to be compared to the source.</param>
        [DebuggerNonUserCode]
        private static int EditDistanceScoreComparison(EditDistanceScore source, EditDistanceScore target)
        {
            return source.Score.CompareTo(target.Score);
        }

        /// <summary>
        ///     Finds the best score from the array of scores, then clears the scores along the axis of the score.
        /// </summary>
        /// <param name="source">An value which will be set to the index (column) of the source with the best match.</param>
        /// <param name="target">An value which will be set to the index (row) of the source with the best match.</param>
        /// <param name="scores">The array of scores to be searched.</param>
        /// <param name="sourceLength">The length of the source array (number of columns).</param>
        /// <param name="targetLength">The length of the target array (number of rows).</param>
        /// <returns>Returns the best score from the array of scores, then clears the scores along the axis of the score.</returns>
        [DebuggerNonUserCode]
        private static int GetBestScore(ref int source, ref int target, int[][] scores, int sourceLength, int targetLength)
        {
            var retVal = 0;
            for (var idxSource = 0; idxSource < sourceLength; idxSource++)
            {
                for (var idxTarget = 0; idxTarget < targetLength; idxTarget++)
                {
                    if (scores[idxSource][idxTarget] > retVal)
                    {
                        retVal = scores[idxSource][idxTarget];
                        source = idxSource;
                        target = idxTarget;
                    }
                }
            }
            return retVal;
        }

        /// <summary>
        ///     Gets the Edit Distance score for strings containing a single word.
        /// </summary>
        /// <param name="source">The source string to compare.</param>
        /// <param name="target">The target string to compare.</param>
        /// <returns>Returns the Edit Distance score for strings containing a single word.</returns>
        [DebuggerNonUserCode]
        private static int GetScore(string source, string target)
        {
            var retVal = 0;

            // Hardcode a score of 90% if one is an abreviation of the other.
            if ((source.Length > target.Length) && (source.StartsWith(target, StringComparison.Ordinal)))
            {
                retVal = 90;
            }
            if ((target.Length > source.Length) && (target.StartsWith(source, StringComparison.Ordinal)))
            {
                retVal = 90;
            }

            // Not an abreviation -- Calc score using Edit Distance.
            if (retVal == 0)
            {
                var editDistance = source.CalculateEditDistance(target);
                var length = Math.Max(source.Length, target.Length);

                retVal = ((length - editDistance) * 100) / length;
            }

            return retVal;
        }

        #endregion

        /// <summary>
        ///     Stores the value and score of a string, used for sorting a string collection by Edit Distance.
        /// </summary>
        private struct EditDistanceScore
        {
            #region Public Properties

            public int Score { get; set; }
            public string Value { get; set; }

            #endregion
        }
    }
}
