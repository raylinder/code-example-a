﻿// ===================================================
// Code Example A / CodeExampleA.Extensions.Core / Utilities.cs
// Created on 06/02/2014 @ 11:45 PM
// ===================================================

namespace CodeExampleA.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Diagnostics.CodeAnalysis;
    using System.Drawing;
    using System.Globalization;
    using System.Text.RegularExpressions;

    /// <summary>
    ///     Contains utility extension methods for System namespace objects.
    /// </summary>
    public static class Utilities
    {
        #region Static Fields

        /// <summary>
        ///     The default language Guid.
        /// </summary>
        private static readonly Guid _defaultLanguage = new Guid("0138aa72-0144-4e3f-972c-2f6760a2f890");

        /// <summary>
        ///     The regular expression used to validate email addresses.
        /// </summary>
        private static readonly Regex _emailRegex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", RegexOptions.Compiled);

        /// <summary>
        ///     The regular expression used to normalize phone numbers.
        /// </summary>
        private static readonly Regex _phoneFixRegex = new Regex(@".*?(\d{3}).*?(\d{3}).*?(\d{4}).*", RegexOptions.Compiled);

        /// <summary>
        ///     The regular expression used to validate phone numbers.
        /// </summary>
        private static readonly Regex _phoneRegex = new Regex(@"^(?:\([2-9]\d{2}\)\ ?|(?:[2-9]\d{2}\-))[2-9]\d{2}\-\d{4}$", RegexOptions.Compiled);

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Calculates the number of Milliseconds for the specified time parameters.
        /// </summary>
        /// <param name="years">The number of years.</param>
        /// <param name="months">The number of months.</param>
        /// <param name="days">The number of days.</param>
        /// <param name="hours">The number of hours.</param>
        /// <param name="minutes">The number of minutes.</param>
        /// <param name="seconds">The number of seconds.</param>
        /// <param name="milliseconds">The number of milliseconds.</param>
        /// <returns></returns>
        public static long CalculateMilliseconds(double years, double months, int days, int hours, int minutes, int seconds, int milliseconds)
        {
            return (((((((((Convert.ToInt64((years + (months / 12)) * 365.25) + days) * 24) + hours) * 60) + minutes) * 60) + seconds) * 1000) + milliseconds);
        }

        /// <summary>
        ///     Converts and object into another type.
        /// </summary>
        /// <param name="value">The type the object is to be converted to.</param>
        /// <typeparam name="T">The type the object is to be converted to.</typeparam>
        /// <remarks>This method differs from Convert.ChangeType in that it handles Nullable and Generic types.</remarks>
        public static T ChangeType<T>(this object value)
        {
            return (T)ChangeType(value, typeof(T), CultureInfo.CurrentCulture);
        }

        /// <summary>
        ///     Converts and object into another type.
        /// </summary>
        /// <param name="value">The object to be converted</param>
        /// <param name="type">The type the object is to be converted to.</param>
        /// <remarks>This method differs from Convert.ChangeType in that it handles Nullable and Generic types.</remarks>
        public static object ChangeType(this object value, Type type)
        {
            return ChangeType(value, type, CultureInfo.CurrentCulture);
        }

        /// <summary>
        ///     Converts and object into another type.
        /// </summary>
        /// <param name="value">The object to be converted</param>
        /// <param name="type">The type the object is to be converted to.</param>
        /// <param name="provider">An object which supplies culture specific formatting information.</param>
        /// <remarks>This method differs from Convert.ChangeType in that it handles Nullable and Generic types.</remarks>
        public static object ChangeType(this object value, Type type, IFormatProvider provider)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            if (value == null && type.IsGenericType)
            {
                return Activator.CreateInstance(type);
            }
            if (value == null)
            {
                return null;
            }
            if (type == value.GetType())
            {
                return value;
            }
            if (!type.IsGenericType || type.IsInterface)
            {
                return Convert.ChangeType(value, type, provider);
            }

            var innerType = type.GetGenericArguments()[0];
            var innerValue = Convert.ChangeType(value, innerType, provider);
            return Activator.CreateInstance(type, new[] { innerValue });
        }

        /// <summary>
        ///     Normalizes a phone number.
        /// </summary>
        /// <param name="phone">The phone number to be normalized.</param>
        public static string FixPhone(this string phone)
        {
            if (string.IsNullOrWhiteSpace(phone))
            {
                return null;
            }
            return _phoneFixRegex.Replace(phone, "$1$2$3");
        }

        /// <summary>
        ///     Determines if the birth date is the default value for System.DateTime.
        /// </summary>
        /// <param name="birthdate">The birth date of the person.</param>
        public static bool IsBirthdateDefault(this DateTime birthdate)
        {
            return birthdate == default(DateTime);
        }

        /// <summary>
        ///     Determines if the Date falls within the day(s) specified.
        /// </summary>
        /// <param name="dateValue">The date to check.</param>
        /// <param name="day">The day(s) of the week.</param>
        /// <returns>Returns true if the date falls within the day(s) specified.</returns>
        public static bool IsDayOfTheWeek(this DateTime dateValue, DaysOfTheWeek day)
        {
            switch (dateValue.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    return (day & DaysOfTheWeek.Sunday) != 0;
                case DayOfWeek.Monday:
                    return (day & DaysOfTheWeek.Monday) != 0;
                case DayOfWeek.Tuesday:
                    return (day & DaysOfTheWeek.Tuesday) != 0;
                case DayOfWeek.Wednesday:
                    return (day & DaysOfTheWeek.Wednesday) != 0;
                case DayOfWeek.Thursday:
                    return (day & DaysOfTheWeek.Thursday) != 0;
                case DayOfWeek.Friday:
                    return (day & DaysOfTheWeek.Friday) != 0;
                case DayOfWeek.Saturday:
                    return (day & DaysOfTheWeek.Saturday) != 0;
                default:
                    return false;
            }
        }

        /// <summary>
        ///     Determines if an email address is valid.
        /// </summary>
        /// <param name="email">The email address to be validated.</param>
        public static bool IsValidEmail(this string email)
        {
            return !string.IsNullOrWhiteSpace(email) && _emailRegex.IsMatch(email);
        }

        /// <summary>
        ///     Determines if a phone number is valid.
        /// </summary>
        /// <param name="phone">The phone number to be validated.</param>
        public static bool IsValidPhone(this string phone)
        {
            return !string.IsNullOrWhiteSpace(phone) && _phoneRegex.IsMatch(phone);
        }

        /// <summary>
        ///     Converts an object to a boolean value.
        /// </summary>
        /// <param name="source">The source object to be converted.</param>
        /// <param name="defaultValue">The value to be returned if the source object is null or cannot be converted.</param>
        [SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", Justification = "Type name is a necessary element of the name of this function")]
        public static bool ToBool(this object source, bool defaultValue = false)
        {
            if (source == null)
            {
                return defaultValue;
            }

            if (source is bool)
            {
                return (bool)source;
            }

            try
            {
                return Convert.ToBoolean(source, CultureInfo.CurrentCulture);
            }
            catch (FormatException)
            {
            }
            catch (InvalidCastException)
            {
            }

            var s = source.ToString();

            bool result;
            if (bool.TryParse(s, out result))
            {
                return result;
            }

            long lresult;
            if (long.TryParse(s, out lresult))
            {
                return lresult != 0;
            }

            return defaultValue;
        }

        /// <summary>
        ///     Converts an object to a byte value.
        /// </summary>
        /// <param name="source">The source object to be converted.</param>
        /// <param name="defaultValue">The value to be returned if the source object is null or cannot be converted.</param>
        [SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", Justification = "Type name is a necessary element of the name of this function")]
        public static byte ToByte(this object source, byte defaultValue = Byte.MinValue)
        {
            if (source == null)
            {
                return defaultValue;
            }

            if (source is byte)
            {
                return (byte)source;
            }

            try
            {
                return Convert.ToByte(source, CultureInfo.CurrentCulture);
            }
            catch (FormatException)
            {
            }
            catch (InvalidCastException)
            {
            }
            catch (OverflowException)
            {
            }

            byte result;
            if (byte.TryParse(source.ToString(), out result))
            {
                return result;
            }

            return defaultValue;
        }

        /// <summary>
        ///     Converts an object to a char value.
        /// </summary>
        /// <param name="source">The source object to be converted.</param>
        /// <param name="defaultValue">The value to be returned if the source object is null or cannot be converted.</param>
        public static char ToChar(this object source, char defaultValue = char.MinValue)
        {
            if (source == null)
            {
                return defaultValue;
            }

            if (source is char)
            {
                return (char)source;
            }

            try
            {
                return Convert.ToChar(source, CultureInfo.CurrentCulture);
            }
            catch (FormatException)
            {
            }
            catch (InvalidCastException)
            {
            }
            catch (OverflowException)
            {
            }

            char result;
            return char.TryParse(source.ToString(), out result) ? result : defaultValue;
        }

        /// <summary>
        ///     Converts an object to a Color.
        /// </summary>
        /// <param name="source">The source object to be converted.</param>
        public static Color ToColor(this object source)
        {
            return ToColor(source, Color.White);
        }

        /// <summary>
        ///     Converts an object to a Color.
        /// </summary>
        /// <param name="source">The source object to be converted.</param>
        /// <param name="defaultValue">The value to be returned if the source object is null or cannot be converted.</param>
        public static Color ToColor(this object source, Color defaultValue)
        {
            if (source == null)
            {
                return defaultValue;
            }

            if (source is Color)
            {
                return (Color)source;
            }

            if (!source.ToString().StartsWith("#", StringComparison.Ordinal) || source.ToString().Length < 6)
            {
                return defaultValue;
            }

            var result = ColorTranslator.FromHtml(source.ToString());
            if (result.IsEmpty)
            {
                return defaultValue;
            }

            return result;
        }

        /// <summary>
        ///     Converts an object to a DateTime value.
        /// </summary>
        /// <param name="source">The source object to be converted.</param>
        public static DateTime ToDateTime(this object source)
        {
            return ToDateTime(source, DateTime.MinValue);
        }

        /// <summary>
        ///     Converts an object to a DateTime value.
        /// </summary>
        /// <param name="source">The source object to be converted.</param>
        /// <param name="defaultValue">The value to be returned if the source object is null or cannot be converted.</param>
        public static DateTime ToDateTime(this object source, DateTime defaultValue)
        {
            if (source == null)
            {
                return defaultValue;
            }

            if (source is DateTime)
            {
                return (DateTime)source;
            }

            try
            {
                return Convert.ToDateTime(source, CultureInfo.CurrentCulture);
            }
            catch (FormatException)
            {
            }
            catch (InvalidCastException)
            {
            }
            catch (OverflowException)
            {
            }

            DateTime result;
            if (DateTime.TryParse(source.ToString(), out result))
            {
                return result;
            }

            return defaultValue;
        }

        /// <summary>
        ///     Converts an object to a decimal value.
        /// </summary>
        /// <param name="source">The source object to be converted.</param>
        /// <param name="defaultValue">The value to be returned if the source object is null or cannot be converted.</param>
        [SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", Justification = "Type name is a necessary element of the name of this function")]
        public static decimal ToDecimal(this object source, decimal defaultValue = decimal.Zero)
        {
            if (source == null)
            {
                return defaultValue;
            }

            if (source is decimal)
            {
                return (decimal)source;
            }

            try
            {
                return Convert.ToDecimal(source, CultureInfo.CurrentCulture);
            }
            catch (FormatException)
            {
            }
            catch (InvalidCastException)
            {
            }
            catch (OverflowException)
            {
            }

            decimal result;
            if (decimal.TryParse(source.ToString(), out result))
            {
                return result;
            }

            return defaultValue;
        }

        /// <summary>
        ///     Converts an object to a double floating point value.
        /// </summary>
        /// <param name="source">The source object to be converted.</param>
        /// <param name="defaultValue">The value to be returned if the source object is null or cannot be converted.</param>
        [SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", Justification = "Type name is a necessary element of the name of this function")]
        public static double ToDouble(this object source, double defaultValue = 0d)
        {
            if (source == null)
            {
                return defaultValue;
            }

            if (source is double)
            {
                return (double)source;
            }

            try
            {
                return Convert.ToDouble(source, CultureInfo.CurrentCulture);
            }
            catch (FormatException)
            {
            }
            catch (InvalidCastException)
            {
            }
            catch (OverflowException)
            {
            }

            double result;
            if (double.TryParse(source.ToString(), out result))
            {
                return result;
            }

            return defaultValue;
        }

        /// <summary>
        ///     Converts an object to a single floating point value.
        /// </summary>
        /// <param name="source">The source object to be converted.</param>
        /// <param name="defaultValue">The value to be returned if the source object is null or cannot be converted.</param>
        [SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", Justification = "Type name is a necessary element of the name of this function")]
        public static float ToFloat(this object source, float defaultValue = 0f)
        {
            if (source == null)
            {
                return defaultValue;
            }

            if (source is float)
            {
                return (float)source;
            }

            try
            {
                return Convert.ToSingle(source, CultureInfo.CurrentCulture);
            }
            catch (FormatException)
            {
            }
            catch (InvalidCastException)
            {
            }
            catch (OverflowException)
            {
            }

            float result;
            if (float.TryParse(source.ToString(), out result))
            {
                return result;
            }

            return defaultValue;
        }

        /// <summary>
        ///     Converts a Nullable Guid to a Guid
        /// </summary>
        /// <param name="source">The source object to be converted.</param>
        public static Guid ToGuid(this Guid? source)
        {
            return ToGuid(source, Guid.Empty);
        }

        /// <summary>
        ///     Converts a Nullable Guid to a Guid
        /// </summary>
        /// <param name="source">The source object to be converted.</param>
        /// <param name="defaultValue">The value to be returned if the source object is null or cannot be converted.</param>
        public static Guid ToGuid(this Guid? source, Guid defaultValue)
        {
            if (source == null)
            {
                return defaultValue;
            }
            return source.Value;
        }

        /// <summary>
        ///     Converts an object to a Guid
        /// </summary>
        /// <param name="source">The source object to be converted.</param>
        public static Guid ToGuid(this object source)
        {
            return ToGuid(source, Guid.Empty);
        }

        /// <summary>
        ///     Converts an object to a Guid
        /// </summary>
        /// <param name="source">The source object to be converted.</param>
        /// <param name="defaultValue">The value to be returned if the source object is null or cannot be converted.</param>
        public static Guid ToGuid(this object source, Guid defaultValue)
        {
            if (source == null)
            {
                return defaultValue;
            }

            if (source is Guid)
            {
                return (Guid)source;
            }

            Guid result;
            if (Guid.TryParse(source.ToString(), out result))
            {
                return result;
            }

            return defaultValue;
        }

        /// <summary>
        ///     Converts an object to a int value.
        /// </summary>
        /// <param name="source">The source object to be converted.</param>
        /// <param name="defaultValue">The value to be returned if the source object is null or cannot be converted.</param>
        [SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", Justification = "Type name is a necessary element of the name of this function")]
        public static int ToInt(this object source, int defaultValue = 0)
        {
            if (source == null)
            {
                return defaultValue;
            }
            int result;

            if (source is int)
            {
                return (int)source;
            }

            try
            {
                return Convert.ToInt32(source, CultureInfo.CurrentCulture);
            }
            catch (FormatException)
            {
            }
            catch (InvalidCastException)
            {
            }
            catch (OverflowException)
            {
            }

            if (int.TryParse(source.ToString(), out result))
            {
                return result;
            }

            return defaultValue;
        }

        /// <summary>
        ///     Converts an object to a Language Identifier.
        /// </summary>
        /// <param name="source">The source object to be converted.</param>
        public static Guid ToLanguage(this object source)
        {
            return ToLanguage(source, _defaultLanguage);
        }

        /// <summary>
        ///     Converts an object to a Language Identifier.
        /// </summary>
        /// <param name="source">The source object to be converted.</param>
        /// <param name="defaultValue">The value to be returned if the source object is null or cannot be converted.</param>
        public static Guid ToLanguage(this object source, Guid defaultValue)
        {
            if (source == null)
            {
                return defaultValue;
            }

            switch (source.ToString().ToUpperInvariant())
            {
                case "FR":
                    return new Guid("5b35408b-2cfe-404e-b779-3152b9921f59");
                case "SP":
                    return new Guid("6f609349-b627-44c3-9afd-31a051c323a0");
                default:
                    return defaultValue;
            }
        }

        /// <summary>
        ///     Converts an object to a long value.
        /// </summary>
        /// <param name="source">The source object to be converted.</param>
        /// <param name="defaultValue">The value to be returned if the source object is null or cannot be converted.</param>
        [SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", Justification = "Type name is a necessary element of the name of this function")]
        public static long ToLong(this object source, long defaultValue = 0L)
        {
            if (source == null)
            {
                return defaultValue;
            }

            if (source is long)
            {
                return (long)source;
            }

            try
            {
                return Convert.ToInt64(source, CultureInfo.CurrentCulture);
            }
            catch (FormatException)
            {
            }
            catch (InvalidCastException)
            {
            }
            catch (OverflowException)
            {
            }

            long result;
            if (long.TryParse(source.ToString(), out result))
            {
                return result;
            }

            return defaultValue;
        }

        /// <summary>
        ///     Converts an enumeration of key/value pairs to a Name Value Collection.
        /// </summary>
        /// <param name="collection">The key value pairs to be converted to a Name Value Collection</param>
        public static NameValueCollection ToNameValueCollection(this IEnumerable<KeyValuePair<string, string>> collection)
        {
            var col = new NameValueCollection();
            col.AddRange(collection);
            return col;
        }

        /// <summary>
        ///     Converts an object to a short value.
        /// </summary>
        /// <param name="source">The source object to be converted.</param>
        /// <param name="defaultValue">The value to be returned if the source object is null or cannot be converted.</param>
        [SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", Justification = "Type name is a necessary element of the name of this function")]
        public static short ToShort(this object source, short defaultValue = 0)
        {
            if (source == null)
            {
                return defaultValue;
            }

            if (source is short)
            {
                return (short)source;
            }

            try
            {
                return Convert.ToInt16(source, CultureInfo.CurrentCulture);
            }
            catch (FormatException)
            {
            }
            catch (InvalidCastException)
            {
            }
            catch (OverflowException)
            {
            }

            short result;
            if (short.TryParse(source.ToString(), out result))
            {
                return result;
            }

            return defaultValue;
        }

        /// <summary>
        ///     Converts an object to a TimeSpan value.
        /// </summary>
        /// <param name="source">The source object to be converted.</param>
        public static TimeSpan ToTimeSpan(this object source)
        {
            return ToTimeSpan(source, TimeSpan.MinValue);
        }

        /// <summary>
        ///     Converts an object to a TimeSpan value.
        /// </summary>
        /// <param name="source">The source object to be converted.</param>
        /// <param name="defaultValue">The value to be returned if the source object is null or cannot be converted.</param>
        public static TimeSpan ToTimeSpan(this object source, TimeSpan defaultValue)
        {
            if (source == null)
            {
                return defaultValue;
            }

            if (source is TimeSpan)
            {
                return (TimeSpan)source;
            }

            TimeSpan result;
            if (TimeSpan.TryParse(source.ToString(), out result))
            {
                return result;
            }

            return defaultValue;
        }

        #endregion

        /// <summary>
        ///     Contains Time values as Milliseconds
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible", Justification = "This is a non-instantiable static class which only contains constants.")]
        public static class Milliseconds
        {
            /// <summary>
            ///     Contains Day values as Milliseconds
            /// </summary>
            [SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible", Justification = "This is a non-instantiable static class which only contains constants.")]
            public static class Days
            {
                #region Constants

                /// <summary>
                ///     The number of milliseconds in Fifty Days
                /// </summary>
                public const long Fifty = 50 * One;

                /// <summary>
                ///     The number of milliseconds in Five Days
                /// </summary>
                public const long Five = 5 * One;

                /// <summary>
                ///     The number of milliseconds in Five-Hundred Days
                /// </summary>
                public const long FiveHundred = 5 * OneHundred;

                /// <summary>
                ///     The number of milliseconds in Four Days
                /// </summary>
                public const long Four = 4 * One;

                /// <summary>
                ///     The number of milliseconds in One Day
                /// </summary>
                public const long One = Hours.One * 24;

                /// <summary>
                ///     The number of milliseconds in One-Hundred Days
                /// </summary>
                public const long OneHundred = 100 * One;

                /// <summary>
                ///     The number of milliseconds in Seventy-Five Days
                /// </summary>
                public const long SeventyFive = 75 * One;

                /// <summary>
                ///     The number of milliseconds in Ten Days
                /// </summary>
                public const long Ten = 10 * One;

                /// <summary>
                ///     The number of milliseconds in Three Days
                /// </summary>
                public const long Three = 3 * One;

                /// <summary>
                ///     The number of milliseconds in Twenty-Five Days
                /// </summary>
                public const long TwentyFive = 25 * One;

                /// <summary>
                ///     The number of milliseconds in Two Days
                /// </summary>
                public const long Two = 2 * One;

                #endregion
            }

            /// <summary>
            ///     Contains Hour values as Milliseconds
            /// </summary>
            [SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible", Justification = "This is a non-instantiable static class which only contains constants.")]
            public static class Hours
            {
                #region Constants

                /// <summary>
                ///     The number of milliseconds in Fifty Hours
                /// </summary>
                public const long Fifty = 50 * One;

                /// <summary>
                ///     The number of milliseconds in Five Hours
                /// </summary>
                public const long Five = 5 * One;

                /// <summary>
                ///     The number of milliseconds in Four Hours
                /// </summary>
                public const long Four = 4 * One;

                /// <summary>
                ///     The number of milliseconds in One Hour
                /// </summary>
                public const long One = Minutes.One * 60;

                /// <summary>
                ///     The number of milliseconds in Seventy-Five Hours
                /// </summary>
                public const long SeventyFive = 75 * One;

                /// <summary>
                ///     The number of milliseconds in Ten Hours
                /// </summary>
                public const long Ten = 10 * One;

                /// <summary>
                ///     The number of milliseconds in Three Hours
                /// </summary>
                public const long Three = 3 * One;

                /// <summary>
                ///     The number of milliseconds in Twenty-Five Hours
                /// </summary>
                public const long TwentyFive = 25 * One;

                /// <summary>
                ///     The number of milliseconds in Two Hours
                /// </summary>
                public const long Two = 2 * One;

                #endregion
            }

            /// <summary>
            ///     Contains Minute values as Milliseconds
            /// </summary>
            [SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible", Justification = "This is a non-instantiable static class which only contains constants.")]
            public static class Minutes
            {
                #region Constants

                /// <summary>
                ///     The number of milliseconds in Fifteen Minutes
                /// </summary>
                public const long Fifteen = 15 * One;

                /// <summary>
                ///     The number of milliseconds in Five Minutes
                /// </summary>
                public const long Five = 5 * One;

                /// <summary>
                ///     The number of milliseconds in Forty-Five Minutes
                /// </summary>
                public const long FortyFive = 45 * One;

                /// <summary>
                ///     The number of milliseconds in Four Minutes
                /// </summary>
                public const long Four = 4 * One;

                /// <summary>
                ///     The number of milliseconds in One Minute
                /// </summary>
                public const long One = Seconds.One * 60;

                /// <summary>
                ///     The number of milliseconds in Ten Minutes
                /// </summary>
                public const long Ten = 10 * One;

                /// <summary>
                ///     The number of milliseconds in Thirty Minutes
                /// </summary>
                public const long Thirty = 30 * One;

                /// <summary>
                ///     The number of milliseconds in Three Minutes
                /// </summary>
                public const long Three = 3 * One;

                /// <summary>
                ///     The number of milliseconds in Two Minutes
                /// </summary>
                public const long Two = 2 * One;

                #endregion
            }

            /// <summary>
            ///     Contains Second values as Milliseconds
            /// </summary>
            [SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible", Justification = "This is a non-instantiable static class which only contains constants.")]
            public static class Seconds
            {
                #region Constants

                /// <summary>
                ///     The number of milliseconds in Fifteen Seconds
                /// </summary>
                public const long Fifteen = 15 * One;

                /// <summary>
                ///     The number of milliseconds in Five Seconds
                /// </summary>
                public const long Five = 5 * One;

                /// <summary>
                ///     The number of milliseconds in Forty-Five Seconds
                /// </summary>
                public const long FortyFive = 45 * One;

                /// <summary>
                ///     The number of milliseconds in Four Seconds
                /// </summary>
                public const long Four = 4 * One;

                /// <summary>
                ///     The number of milliseconds in One Second
                /// </summary>
                public const long One = 1000;

                /// <summary>
                ///     The number of milliseconds in Ten Seconds
                /// </summary>
                public const long Ten = 10 * One;

                /// <summary>
                ///     The number of milliseconds in Thirty Seconds
                /// </summary>
                public const long Thirty = 30 * One;

                /// <summary>
                ///     The number of milliseconds in Three Seconds
                /// </summary>
                public const long Three = 3 * One;

                /// <summary>
                ///     The number of milliseconds in Two Seconds
                /// </summary>
                public const long Two = 2 * One;

                #endregion
            }
        }
    }

    #region Days of the Week flags

    /// <summary>
    ///     Day of the Week.
    /// </summary>
    [Flags]
    [SuppressMessage("Microsoft.Naming", "CA1714:FlagsEnumsShouldHavePluralNames", Justification = "Name is plural.")]
    public enum DaysOfTheWeek
    {
        /// <summary>
        ///     No days selected.
        /// </summary>
        None = 0,

        /// <summary>
        ///     Sunday
        /// </summary>
        Sunday = 1,

        /// <summary>
        ///     Monday
        /// </summary>
        Monday = 2,

        /// <summary>
        ///     Tuesday
        /// </summary>
        Tuesday = 4,

        /// <summary>
        ///     Wednesday
        /// </summary>
        Wednesday = 8,

        /// <summary>
        ///     Thursday
        /// </summary>
        Thursday = 16,

        /// <summary>
        ///     Friday
        /// </summary>
        Friday = 32,

        /// <summary>
        ///     Monday through Friday
        /// </summary>
        Weekdays = 62,

        /// <summary>
        ///     Saturday
        /// </summary>
        Saturday = 64,

        /// <summary>
        ///     Saturday and Sunday
        /// </summary>
        Weekends = 65,

        /// <summary>
        ///     Sunday through Saturday
        /// </summary>
        Everyday = 127
    }

    #endregion
}
