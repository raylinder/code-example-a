﻿// ===================================================
// Code Example A / CodeExampleA.Extensions.Data / Utilities.cs
// Created on 06/02/2014 @ 11:46 PM
// ===================================================

namespace CodeExampleA.Extensions.Data
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Linq;

    public static class DataExtensions
    {
        #region Static Fields

        /// <summary>
        ///     Stores the DbType translations.
        /// </summary>
        private static Dictionary<Type, DbType> _dbTypeConversions = new Dictionary<Type, DbType>();

        /// <summary>
        ///     Stores the SqlDbType translations.
        /// </summary>
        private static Dictionary<Type, SqlDbType> _sqlDbTypeConversions = new Dictionary<Type, SqlDbType>();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Static Constructor
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Initializing two translation dictionaries, not convenient to do this on the field declaration")]
        static DataExtensions()
        {
            #region SqlDbType and DbType Conversion tables

            _sqlDbTypeConversions.Add(typeof(bool), SqlDbType.Bit);
            _sqlDbTypeConversions.Add(typeof(byte), SqlDbType.TinyInt);
            _sqlDbTypeConversions.Add(typeof(byte[]), SqlDbType.VarBinary);
            _sqlDbTypeConversions.Add(typeof(DateTime), SqlDbType.DateTime);
            _sqlDbTypeConversions.Add(typeof(decimal), SqlDbType.Decimal);
            _sqlDbTypeConversions.Add(typeof(double), SqlDbType.Float);
            _sqlDbTypeConversions.Add(typeof(Guid), SqlDbType.UniqueIdentifier);
            _sqlDbTypeConversions.Add(typeof(short), SqlDbType.SmallInt);
            _sqlDbTypeConversions.Add(typeof(int), SqlDbType.Int);
            _sqlDbTypeConversions.Add(typeof(long), SqlDbType.BigInt);
            _sqlDbTypeConversions.Add(typeof(string), SqlDbType.VarChar);
            _sqlDbTypeConversions.Add(typeof(bool?), SqlDbType.Bit);
            _sqlDbTypeConversions.Add(typeof(byte?), SqlDbType.TinyInt);
            _sqlDbTypeConversions.Add(typeof(DateTime?), SqlDbType.DateTime);
            _sqlDbTypeConversions.Add(typeof(decimal?), SqlDbType.Decimal);
            _sqlDbTypeConversions.Add(typeof(double?), SqlDbType.Float);
            _sqlDbTypeConversions.Add(typeof(Guid?), SqlDbType.UniqueIdentifier);
            _sqlDbTypeConversions.Add(typeof(short?), SqlDbType.SmallInt);
            _sqlDbTypeConversions.Add(typeof(int?), SqlDbType.Int);
            _sqlDbTypeConversions.Add(typeof(long?), SqlDbType.BigInt);

            _dbTypeConversions.Add(typeof(bool), DbType.Boolean);
            _dbTypeConversions.Add(typeof(byte), DbType.Double);
            _dbTypeConversions.Add(typeof(byte[]), DbType.Binary);
            _dbTypeConversions.Add(typeof(DateTime), DbType.DateTime);
            _dbTypeConversions.Add(typeof(decimal), DbType.Decimal);
            _dbTypeConversions.Add(typeof(double), DbType.Double);
            _dbTypeConversions.Add(typeof(Guid), DbType.Guid);
            _dbTypeConversions.Add(typeof(short), DbType.Int16);
            _dbTypeConversions.Add(typeof(int), DbType.Int32);
            _dbTypeConversions.Add(typeof(long), DbType.Int64);
            _dbTypeConversions.Add(typeof(string), DbType.String);
            _dbTypeConversions.Add(typeof(bool?), DbType.Boolean);
            _dbTypeConversions.Add(typeof(byte?), DbType.Double);
            _dbTypeConversions.Add(typeof(DateTime?), DbType.DateTime);
            _dbTypeConversions.Add(typeof(decimal?), DbType.Decimal);
            _dbTypeConversions.Add(typeof(double?), DbType.Double);
            _dbTypeConversions.Add(typeof(Guid?), DbType.Guid);
            _dbTypeConversions.Add(typeof(short?), DbType.Int16);
            _dbTypeConversions.Add(typeof(int?), DbType.Int32);
            _dbTypeConversions.Add(typeof(long?), DbType.Int64);

            #endregion
        }

        #endregion

        #region Public Methods and Operators

        public static SqlParameter AddWithValue_Nullable<T>(this SqlParameterCollection parameters, string name, object value)
        {
            if (value == null)
            {
                var parm = parameters.Add(new SqlParameter(name, DBNull.Value));
                parm.SqlDbType = typeof(T).ToSqlDbType();
                parm.IsNullable = true;
                if (parm.SqlDbType == SqlDbType.VarBinary)
                {
                    parm.Size = -1;
                }
                return parm;
            }

            return parameters.AddWithValue(name, value);
        }

        public static SqlParameter AddWithValue_Nullable(this SqlParameterCollection parameters, string name, object value)
        {
            if (value == null)
            {
                var parm = parameters.Add(new SqlParameter(name, DBNull.Value));
                parm.IsNullable = true;
                return parm;
            }

            return parameters.AddWithValue(name, value);
        }

        public static object DbNullable(this object obj)
        {
            if (obj == DBNull.Value)
            {
                return null;
            }

            return obj;
        }

        /// <summary>
        ///     Retrieves a string from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The string value of the data contained within the Data Reader at the specified column.</returns>
        public static byte[] GetBinaryOrDefault(this IDataRecord reader, string columnName, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetBinaryOrDefault(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName));
        }

        /// <summary>
        ///     Retrieves a string from a Data Reader from the specified column, by column index.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <returns>The string value of the data contained within the Data Reader at the specified column.</returns>
        public static byte[] GetBinaryOrDefault(this IDataRecord reader, int index)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if ((index == -1) || reader.IsDBNull(index))
            {
                return new byte[0];
            }

            return reader.GetValue(index) as byte[];
        }

        /// <summary>
        ///     Retrieves a string from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The string value of the data contained within the Data Reader at the specified column.</returns>
        public static byte[] GetBinaryOrNull(this IDataRecord reader, string columnName, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetBinaryOrNull(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName));
        }

        /// <summary>
        ///     Retrieves a string from a Data Reader from the specified column, by column index.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <returns>The string value of the data contained within the Data Reader at the specified column.</returns>
        public static byte[] GetBinaryOrNull(this IDataRecord reader, int index)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if ((index == -1) || reader.IsDBNull(index))
            {
                return null;
            }

            return reader.GetValue(index) as byte[];
        }

        /// <summary>
        ///     Retrieves a boolean from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The boolean value of the data contained within the Data Reader at the specified column.</returns>
        public static bool GetBooleanOrDefault(this IDataRecord reader, string columnName, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetBooleanOrDefault(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName));
        }

        /// <summary>
        ///     Retrieves a boolean from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <returns>The boolean value of the data contained within the Data Reader at the specified column.</returns>
        public static bool GetBooleanOrDefault(this IDataRecord reader, int index)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if ((index == -1) || reader.IsDBNull(index))
            {
                return default(bool);
            }

            return reader.GetBoolean(index);
        }

        /// <summary>
        ///     Retrieves a boolean from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The boolean value of the data contained within the Data Reader at the specified column.</returns>
        public static bool? GetBooleanOrNull(this IDataRecord reader, string columnName, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetBooleanOrNull(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName));
        }

        /// <summary>
        ///     Retrieves a boolean from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <returns>The boolean value of the data contained within the Data Reader at the specified column.</returns>
        public static bool? GetBooleanOrNull(this IDataRecord reader, int index)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if ((index == -1) || reader.IsDBNull(index))
            {
                return null;
            }

            return reader.GetBoolean(index);
        }

        /// <summary>
        ///     Retrieves a boolean from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The byte value of the data contained within the Data Reader at the specified column.</returns>
        public static byte GetByteOrDefault(this IDataRecord reader, string columnName, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetByteOrDefault(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName));
        }

        /// <summary>
        ///     Retrieves a boolean from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <returns>The byte value of the data contained within the Data Reader at the specified column.</returns>
        public static byte GetByteOrDefault(this IDataRecord reader, int index)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if ((index == -1) || reader.IsDBNull(index))
            {
                return default(byte);
            }

            return reader.GetByte(index);
        }

        /// <summary>
        ///     Retrieves a byte from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The byte value of the data contained within the Data Reader at the specified column.</returns>
        public static byte? GetByteOrNull(this IDataRecord reader, string columnName, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetByteOrNull(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName));
        }

        /// <summary>
        ///     Retrieves a byte from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <returns>The byte value of the data contained within the Data Reader at the specified column.</returns>
        public static byte? GetByteOrNull(this IDataRecord reader, int index)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if ((index == -1) || reader.IsDBNull(index))
            {
                return null;
            }

            return reader.GetByte(index);
        }

        /// <summary>
        ///     Retrieves a Date/Time from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The DateTime value of the data contained within the Data Reader at the specified column.</returns>
        public static DateTime GetDateTimeOrDefault(this IDataRecord reader, string columnName, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetDateTimeOrDefault(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName), CultureInfo.InvariantCulture);
        }

        /// <summary>
        ///     Retrieves a Date/Time from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <returns>The DateTime value of the data contained within the Data Reader at the specified column.</returns>
        public static DateTime GetDateTimeOrDefault(this IDataRecord reader, int index)
        {
            return GetDateTimeOrDefault(reader, index, CultureInfo.InvariantCulture);
        }

        /// <summary>
        ///     Retrieves a Date/Time from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="provider">An object that supplies culture-specific format information.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The DateTime value of the data contained within the Data Reader at the specified column.</returns>
        public static DateTime GetDateTimeOrDefault(this IDataRecord reader, string columnName, IFormatProvider provider, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetDateTimeOrDefault(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName), provider);
        }

        /// <summary>
        ///     Retrieves a Date/Time from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <param name="provider">An object that supplies culture-specific format information.</param>
        /// <returns>The DateTime value of the data contained within the Data Reader at the specified column.</returns>
        public static DateTime GetDateTimeOrDefault(this IDataRecord reader, int index, IFormatProvider provider)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if (provider == null)
            {
                throw new ArgumentNullException("provider");
            }
            if ((index == -1) || reader.IsDBNull(index))
            {
                return default(DateTime);
            }

            try
            {
                return reader.GetDateTime(index);
            }
            catch (InvalidCastException)
            {
                var dateValue = reader.GetString(index);
                try
                {
                    return DateTime.Parse(dateValue, provider);
                }
                catch (FormatException)
                {
                    var year = int.Parse(dateValue.Substring(0, 4), provider);
                    var month = int.Parse(dateValue.Substring(4, 2), provider);
                    var day = int.Parse(dateValue.Substring(6, 2), provider);
                    return new DateTime(year, month, day);
                }
            }
        }

        /// <summary>
        ///     Retrieves a Date/Time from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The DateTime value of the data contained within the Data Reader at the specified column.</returns>
        public static DateTime? GetDateTimeOrNull(this IDataRecord reader, string columnName, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetDateTimeOrNull(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName), CultureInfo.InvariantCulture);
        }

        /// <summary>
        ///     Retrieves a Date/Time from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <returns>The DateTime value of the data contained within the Data Reader at the specified column.</returns>
        public static DateTime? GetDateTimeOrNull(this IDataRecord reader, int index)
        {
            return GetDateTimeOrNull(reader, index, CultureInfo.InvariantCulture);
        }

        /// <summary>
        ///     Retrieves a Date/Time from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="provider">An object that supplies culture-specific format information.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The DateTime value of the data contained within the Data Reader at the specified column.</returns>
        public static DateTime? GetDateTimeOrNull(this IDataRecord reader, string columnName, IFormatProvider provider, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetDateTimeOrNull(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName), provider);
        }

        /// <summary>
        ///     Retrieves a Date/Time from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <param name="provider">An object that supplies culture-specific format information.</param>
        /// <returns>The DateTime value of the data contained within the Data Reader at the specified column.</returns>
        public static DateTime? GetDateTimeOrNull(this IDataRecord reader, int index, IFormatProvider provider)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if (provider == null)
            {
                throw new ArgumentNullException("provider");
            }
            if ((index == -1) || reader.IsDBNull(index))
            {
                return null;
            }

            try
            {
                return reader.GetDateTime(index);
            }
            catch (InvalidCastException)
            {
                var dateValue = reader.GetString(index);
                try
                {
                    return DateTime.Parse(dateValue, provider);
                }
                catch (FormatException)
                {
                    // Assume yyyyMMdd format
                    var year = int.Parse(dateValue.Substring(0, 4), provider);
                    var month = int.Parse(dateValue.Substring(4, 2), provider);
                    var day = int.Parse(dateValue.Substring(6, 2), provider);
                    return new DateTime(year, month, day);
                }
            }
        }

        /// <summary>
        ///     Retrieves a double from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The value of the data contained within the Data Reader at the specified column.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", Justification = "Double is a necessary part of this method name.")]
        public static double GetDoubleOrDefault(this IDataRecord reader, string columnName, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetDoubleOrDefault(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName));
        }

        /// <summary>
        ///     Retrieves a double from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <returns>The value of the data contained within the Data Reader at the specified column.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", Justification = "Double is a necessary part of this method name.")]
        public static double GetDoubleOrDefault(this IDataRecord reader, int index)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if ((index == -1) || reader.IsDBNull(index))
            {
                return default(double);
            }

            return reader.GetDouble(index);
        }

        /// <summary>
        ///     Retrieves a double from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The value of the data contained within the Data Reader at the specified column.</returns>
        public static double? GetDoubleOrNull(this IDataRecord reader, string columnName, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetDoubleOrNull(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName));
        }

        /// <summary>
        ///     Retrieves a double from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <returns>The value of the data contained within the Data Reader at the specified column.</returns>
        public static double? GetDoubleOrNull(this IDataRecord reader, int index)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if ((index == -1) || reader.IsDBNull(index))
            {
                return null;
            }

            return reader.GetDouble(index);
        }

        /// <summary>
        ///     Gets a map of the fields contained in the data reader.
        /// </summary>
        /// <param name="reader">The data reader containing a result set to be mapped.</param>
        public static List<string> GetFieldMap(this IDataRecord reader)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            var retVal = new List<string>();
            for (var idx = 0; idx < reader.FieldCount; idx++)
            {
                retVal.Add(reader.GetName(idx));
            }
            return retVal;
        }

        public static List<string> GetFieldMap(this IDataReader reader)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            var retVal = new List<string>();
            for (var idx = 0; idx < reader.FieldCount; idx++)
            {
                retVal.Add(reader.GetName(idx));
            }
            return retVal;
        }

        /// <summary>
        ///     Retrieves a object from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The object value of the data contained within the Data Reader at the specified column.</returns>
        public static Type GetFieldType(this IDataRecord reader, string columnName, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return reader.GetFieldType(fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName));
        }

        /// <summary>
        ///     Retrieves a double from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The value of the data contained within the Data Reader at the specified column.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", Justification = "Float is a necessary part of this method name.")]
        public static float GetFloatOrDefault(this IDataRecord reader, string columnName, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetFloatOrDefault(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName));
        }

        /// <summary>
        ///     Retrieves a double from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <returns>The value of the data contained within the Data Reader at the specified column.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", Justification = "Float is a necessary part of this method name.")]
        public static float GetFloatOrDefault(this IDataRecord reader, int index)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if ((index == -1) || reader.IsDBNull(index))
            {
                return default(float);
            }

            return reader.GetFloat(index);
        }

        /// <summary>
        ///     Retrieves a double from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The value of the data contained within the Data Reader at the specified column.</returns>
        public static float? GetFloatOrNull(this IDataRecord reader, string columnName, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetFloatOrNull(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName));
        }

        /// <summary>
        ///     Retrieves a double from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <returns>The value of the data contained within the Data Reader at the specified column.</returns>
        public static float? GetFloatOrNull(this IDataRecord reader, int index)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if ((index == -1) || reader.IsDBNull(index))
            {
                return null;
            }

            return reader.GetFloat(index);
        }

        /// <summary>
        ///     Retrieves a boolean from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The boolean value of the data contained within the Data Reader at the specified column.</returns>
        public static Guid GetGuidOrDefault(this IDataRecord reader, string columnName, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetGuidOrDefault(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName));
        }

        /// <summary>
        ///     Retrieves a boolean from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <returns>The boolean value of the data contained within the Data Reader at the specified column.</returns>
        public static Guid GetGuidOrDefault(this IDataRecord reader, int index)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if ((index == -1) || reader.IsDBNull(index))
            {
                return default(Guid);
            }

            return reader.GetGuid(index);
        }

        /// <summary>
        ///     Retrieves a Guid from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The Guid value of the data contained within the Data Reader at the specified column.</returns>
        public static Guid? GetGuidOrNull(this IDataRecord reader, string columnName, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetGuidOrNull(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName));
        }

        /// <summary>
        ///     Retrieves a Guid from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <returns>The Guid value of the data contained within the Data Reader at the specified column.</returns>
        public static Guid? GetGuidOrNull(this IDataRecord reader, int index)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if ((index == -1) || reader.IsDBNull(index))
            {
                return null;
            }

            return reader.GetGuid(index);
        }

        /// <summary>
        ///     Retrieves an integer from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The integer value of the data contained within the Data Reader at the specified column.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", Justification = "Int is a necessary part of this method name.")]
        public static short GetInt16OrDefault(this IDataRecord reader, string columnName, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetInt16OrDefault(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName));
        }

        /// <summary>
        ///     Retrieves an integer from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <returns>The integer value of the data contained within the Data Reader at the specified column.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", Justification = "Int is a necessary part of this method name.")]
        public static short GetInt16OrDefault(this IDataRecord reader, int index)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if ((index == -1) || reader.IsDBNull(index))
            {
                return default(short);
            }

            return reader.GetInt16(index);
        }

        /// <summary>
        ///     Retrieves a short from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The short value of the data contained within the Data Reader at the specified column.</returns>
        public static short? GetInt16OrNull(this IDataRecord reader, string columnName, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetInt16OrNull(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName));
        }

        /// <summary>
        ///     Retrieves a short from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <returns>The short value of the data contained within the Data Reader at the specified column.</returns>
        public static short? GetInt16OrNull(this IDataRecord reader, int index)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if ((index == -1) || reader.IsDBNull(index))
            {
                return null;
            }

            return reader.GetInt16(index);
        }

        /// <summary>
        ///     Retrieves an integer from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The integer value of the data contained within the Data Reader at the specified column.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", Justification = "Int is a necessary part of this method name.")]
        public static int GetInt32OrDefault(this IDataRecord reader, string columnName, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetInt32OrDefault(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName));
        }

        /// <summary>
        ///     Retrieves an integer from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <returns>The integer value of the data contained within the Data Reader at the specified column.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", Justification = "Int is a necessary part of this method name.")]
        public static int GetInt32OrDefault(this IDataRecord reader, int index)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if ((index == -1) || reader.IsDBNull(index))
            {
                return default(int);
            }

            return reader.GetInt32(index);
        }

        /// <summary>
        ///     Retrieves an integer from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The integer value of the data contained within the Data Reader at the specified column.</returns>
        public static int? GetInt32OrNull(this IDataRecord reader, string columnName, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetInt32OrNull(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName));
        }

        /// <summary>
        ///     Retrieves an integer from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <returns>The integer value of the data contained within the Data Reader at the specified column.</returns>
        public static int? GetInt32OrNull(this IDataRecord reader, int index)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if ((index == -1) || reader.IsDBNull(index))
            {
                return null;
            }

            return reader.GetInt32(index);
        }

        /// <summary>
        ///     Retrieves an integer from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The integer value of the data contained within the Data Reader at the specified column.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", Justification = "Long is a necessary part of this method name.")]
        public static long GetInt64OrDefault(this IDataRecord reader, string columnName, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetInt64OrDefault(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName));
        }

        /// <summary>
        ///     Retrieves an integer from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <returns>The integer value of the data contained within the Data Reader at the specified column.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", Justification = "Long is a necessary part of this method name.")]
        public static long GetInt64OrDefault(this IDataRecord reader, int index)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if ((index == -1) || reader.IsDBNull(index))
            {
                return default(long);
            }

            return reader.GetInt64(index);
        }

        /// <summary>
        ///     Retrieves an integer from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The integer value of the data contained within the Data Reader at the specified column.</returns>
        public static long? GetInt64OrNull(this IDataRecord reader, string columnName, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetInt64OrNull(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName));
        }

        /// <summary>
        ///     Retrieves an integer from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <returns>The integer value of the data contained within the Data Reader at the specified column.</returns>
        public static long? GetInt64OrNull(this IDataRecord reader, int index)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if ((index == -1) || reader.IsDBNull(index))
            {
                return null;
            }

            return reader.GetInt64(index);
        }

        /// <summary>
        ///     Retrieves a string array from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="delimiter">The delimiting character of the array.</param>
        /// <returns>The string array of the data contained within the Data Reader at the specified column.</returns>
        public static string[] GetStringArrayOrNull(this IDataRecord reader, string columnName, char delimiter)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetStringArrayOrNull(reader, reader.GetOrdinal(columnName), delimiter);
        }

        /// <summary>
        ///     Retrieves a string array from a Data Reader from the specified column, by column index.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <param name="delimiter">The delimiting character of the array.</param>
        /// <returns>The string array of the data contained within the Data Reader at the specified column.</returns>
        public static string[] GetStringArrayOrNull(this IDataRecord reader, int index, char delimiter)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if ((index == -1) || reader.IsDBNull(index))
            {
                return null;
            }

            return reader.GetString(index).Split(delimiter);
        }

        /// <summary>
        ///     Retrieves a string from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The string value of the data contained within the Data Reader at the specified column.</returns>
        public static string GetStringOrDefault(this IDataRecord reader, string columnName, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetStringOrDefault(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName));
        }

        /// <summary>
        ///     Retrieves a string from a Data Reader from the specified column, by column index.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <returns>The string value of the data contained within the Data Reader at the specified column.</returns>
        public static string GetStringOrDefault(this IDataRecord reader, int index)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if ((index == -1) || reader.IsDBNull(index))
            {
                return string.Empty;
            }

            return reader.GetString(index).Trim();
        }

        /// <summary>
        ///     Retrieves a string from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The string value of the data contained within the Data Reader at the specified column.</returns>
        public static string GetStringOrNull(this IDataRecord reader, string columnName, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetStringOrNull(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName));
        }

        /// <summary>
        ///     Retrieves a string from a Data Reader from the specified column, by column index.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <returns>The string value of the data contained within the Data Reader at the specified column.</returns>
        public static string GetStringOrNull(this IDataRecord reader, int index)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if ((index == -1) || reader.IsDBNull(index))
            {
                return null;
            }

            return reader.GetString(index).Trim();
        }

        /// <summary>
        ///     Retrieves a object from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="columnName">The name of the specified column.</param>
        /// <param name="fieldMap">A list of column names for the current result set of the Data Reader.</param>
        /// <returns>The object value of the data contained within the Data Reader at the specified column.</returns>
        public static object GetValueOrNull(this IDataRecord reader, string columnName, List<string> fieldMap = null)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return GetValueOrNull(reader, fieldMap == null ? reader.GetOrdinal(columnName) : fieldMap.IndexOf(columnName));
        }

        /// <summary>
        ///     Retrieves a object from a Data Reader from the specified column, by column name.
        /// </summary>
        /// <param name="reader">The Data Reader containing the data to retrieve.</param>
        /// <param name="index">The index of the specified column.</param>
        /// <returns>The object value of the data contained within the Data Reader at the specified column.</returns>
        public static object GetValueOrNull(this IDataRecord reader, int index)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if ((index == -1) || reader.IsDBNull(index))
            {
                return null;
            }

            return reader.GetValue(index);
        }

        public static bool HasColumn(this IDataReader dr, string columnName)
        {
            return dr.GetFieldMap().Any(x => x.Equals(columnName, StringComparison.InvariantCultureIgnoreCase));
        }

        public static bool HasColumnValue(this IDataReader dr, string columnName)
        {
            return dr.GetFieldMap().Any(x => x.Equals(columnName, StringComparison.InvariantCultureIgnoreCase)) && dr[columnName] != DBNull.Value;
        }

        /// <summary>
        ///     Determines if a Parameter is an Input Parameter
        /// </summary>
        /// <param name="parameter">The Db Parameter.</param>
        public static bool IsInputParameter(this DbParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException("parameter");
            }

            return ((parameter.Direction & ParameterDirection.Input) == ParameterDirection.Input);
        }

        /// <summary>
        ///     Determines if a Parameter is an Output Parameter
        /// </summary>
        /// <param name="parameter">The Db Parameter.</param>
        public static bool IsOutputParameter(this DbParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException("parameter");
            }

            return ((parameter.Direction & ParameterDirection.Output) == ParameterDirection.Output);
        }

        /// <summary>
        ///     Determines if a Parameter is a Return Value Parameter
        /// </summary>
        /// <param name="parameter">The Db Parameter.</param>
        public static bool IsReturnParameter(this DbParameter parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException("parameter");
            }

            return ((parameter.Direction & ParameterDirection.ReturnValue) == ParameterDirection.ReturnValue);
        }

        /// <summary>
        ///     Sets the Value property of the Db Parameter if the value is not null, otherwise sets the Value property to DbNull
        /// </summary>
        /// <typeparam name="T">The type of the Db Parameter value</typeparam>
        /// <param name="parameter">The Db Parameter</param>
        /// <param name="value">The value to be set.</param>
        public static void SetValueOrDbNull<T>(this DbParameter parameter, T value)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException("parameter");
            }

            if ((value == null) || ((typeof(T) == typeof(Guid)) && Guid.Empty.Equals(value.ToGuid())))
            {
                parameter.Value = DBNull.Value;
            }
            else
            {
                parameter.Value = value;
            }
        }

        /// <summary>
        ///     Converts a System Type to a DbType
        /// </summary>
        /// <param name="type">The System Type to be converted to a DbType</param>
        /// <returns>Returns the DbType equivelant of the specified type.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "Db", Justification = "Case matches that of Microsoft Framework's DbType")]
        public static DbType ToDbType(this Type type)
        {
            if ((type != null) && _dbTypeConversions.ContainsKey(type))
            {
                return _dbTypeConversions[type];
            }
            return DbType.Object;
        }

        /// <summary>
        ///     Converts a System Type to a SqlDbType
        /// </summary>
        /// <param name="type">The System Type to be converted to a SqlDbType</param>
        /// <returns>Returns the SqlDbType equivelant of the specified type.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "Db", Justification = "Case matches that of Microsoft Framework's SqlDbType")]
        public static SqlDbType ToSqlDbType(this Type type)
        {
            if ((type != null) && _sqlDbTypeConversions.ContainsKey(type))
            {
                return _sqlDbTypeConversions[type];
            }
            return SqlDbType.Variant;
        }

        /// <summary>
        ///     Peforms an action on a data reader as long as the data reader has rows.
        /// </summary>
        /// <param name="reader">The data reader containing rows to be read.</param>
        /// <param name="action">The action to be executed while reader has rows.</param>
        public static void WhileRead(this IDataReader reader, Action<IDataReader> action)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            while (reader.Read())
            {
                action(reader);
            }
        }

        /// <summary>
        ///     Peforms an action on a data reader as long as the data reader has rows.
        /// </summary>
        /// <param name="reader">The data reader containing rows to be read.</param>
        /// <param name="action">The action to be executed while reader has rows.</param>
        /// <param name="match">The predicate used to determine if the action should be performed on the given row.</param>
        public static void WhileReadIf(this IDataReader reader, Predicate<IDataReader> match, Action<IDataReader> action)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if (match == null)
            {
                throw new ArgumentNullException("match");
            }
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            while (reader.Read())
            {
                if (match(reader))
                {
                    action(reader);
                }
            }
        }

        #endregion
    }
}
