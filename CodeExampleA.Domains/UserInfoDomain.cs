﻿// ===================================================
// Code Example A / CodeExampleA.Domains / UserInfoDomain.cs
// Created on 06/03/2014 @ 9:16 PM
// ===================================================

namespace CodeExampleA.Domains
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CodeExampleA.Entities;
    using CodeExampleA.Repositories;

    public sealed class UserInfoDomain : IUserInfoDomain
    {
        #region Fields

        private readonly IUserInfoRepository _userInfoRepository;

        #endregion

        #region Constructors and Destructors

        public UserInfoDomain(IUserInfoRepository userInfoRepository)
        {
            _userInfoRepository = userInfoRepository;
        }

        public UserInfoDomain()
        {
        }

        #endregion

        #region Public Methods and Operators

        public UserInfo AddUserInfo(string firstName, string lastName, int age)
        {
            Guid? id;

            var auditRepository = new UserInfoRepository();
            auditRepository.AddUserInfo(firstName, lastName, age, out id);

            if (id == null)
            {
                throw new Exception();
            }

            return new UserInfo { id = id.GetValueOrDefault() };
        }

        public List<UserInfo> GetAllUserInfo()
        {
            return _userInfoRepository.GetAllUserInfo().ToList();
        }

        #endregion
    }
}
