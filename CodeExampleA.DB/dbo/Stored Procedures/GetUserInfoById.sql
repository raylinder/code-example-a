﻿create PROCEDURE [dbo].[GetUserInfoById]
	@id uniqueidentifier
AS
BEGIN
	select firstName, lastName, age 
	from [CodeExampleA].[dbo].[UserInfo]
	where id = @id
END