﻿create PROCEDURE [dbo].[AddUserInfo]
	@firstName varchar(50) = '',
	@lastName varchar(50) = '',
	@age int = 0,
	@id uniqueidentifier output

AS
BEGIN
	insert into [CodeExampleA].[dbo].[UserInfo] (firstName, lastName, age)
	values (@firstName, @lastName, @age)

	set @id = inserted.id
END