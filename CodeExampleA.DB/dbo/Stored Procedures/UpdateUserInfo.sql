﻿CREATE PROCEDURE [dbo].[UpdateUserInfo]
	@id uniqueidentifier,
	@firstName varchar(50) = '',
	@lastName varchar(50) = '',
	@age int = 0
AS
BEGIN
	update [CodeExampleA].[dbo].[UserInfo]
	set firstName = @firstName, lastName = @lastName, age = @age
	where id = @id
END