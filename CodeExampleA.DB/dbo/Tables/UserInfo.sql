﻿CREATE TABLE [dbo].[UserInfo] (
    [id]        UNIQUEIDENTIFIER NOT NULL,
    [firstName] VARCHAR (50)     NOT NULL,
    [lastName]  VARCHAR (50)     NOT NULL,
    [age]       INT              NOT NULL,
    CONSTRAINT [PK_UserInfo] PRIMARY KEY CLUSTERED ([id] ASC)
);

